package com.suixingpay.controller;

import com.suixingpay.core.orm.entity.User;
import com.suixingpay.core.service.HelloRemote;
import com.suixingpay.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by gz on 2017/11/7
 */
@Controller
public class UserController {

    @Autowired
    HelloRemote HelloRemote;

    @Autowired
    private UserService userService;

    @GetMapping("/users/{id}")
    public User findById(@PathVariable(value = "id") String id) {
        return userService.findOne(id);
    }

    @GetMapping("/users")
    public ModelAndView findAll() {
        return new ModelAndView("userlist").addObject("users", userService.findAll());
    }

    @RequestMapping("/homepage")
    public void test() {
        System.out.println("qqqqqqq");
    }

    @RequestMapping("/hello/{name}")
    public void index(@PathVariable("name") String name) {
        System.out.println("test");
        System.out.println(HelloRemote.hello(name));
    }
}
