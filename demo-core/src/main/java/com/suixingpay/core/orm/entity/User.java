package com.suixingpay.core.orm.entity;

import com.suixingpay.common.core.orm.domain.Domain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by gz on 2017/11/7
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Domain {

    private String id;

    private String name;
}
