/**
 * All rights Reserved, Designed By Suixingpay.
 *
 * @author: liyang[li_yang@suixingpay.com]
 * @date: 2017年10月25日 上午10:09:22
 * @Copyright ©2017 Suixingpay. All rights reserved.
 * 注意：本内容仅限于随行付支付有限公司内部传阅，禁止外泄以及用于其他的商业用途。
 */
package com.suixingpay.core.service.impl;

import com.suixingpay.common.core.orm.dao.GenericDao;
import com.suixingpay.common.core.service.AbstractService;
import com.suixingpay.core.orm.entity.User;
import com.suixingpay.core.orm.mapper.UserMapper;
import com.suixingpay.core.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by gz on 2017/11/7
 */
@Service
@Slf4j
public class UserServiceImpl extends AbstractService<User, String> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    protected GenericDao<User, String> getDao() {
        return userMapper;
    }

    @Override
    public User findOne(String id) {
        log.info("查询User id =[{}]", id);
        return this.userMapper.findOne(id);
    }


}
